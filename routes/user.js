var express = require('express');
var router = express.Router();
const UsersController = require('../controllers/users');

router.post("/", UsersController.create_user);
router.delete("/", UsersController.delete_user);
router.get("/", UsersController.get_users);

module.exports = router;