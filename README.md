# IOU

REST API that keeps track of user borrowing between peers. The API exposes four endpoints:

1. /users - a GET method that retrieves a list of select users. Sample request payload {"users": ["john", "jay"]}
2. /add - a POST method that accepts the username as a parameter and creates the users' account/ record. Sample request payload {"user": "jack"}
3. /iou - another POST method that accepts the names of the lender and borrower together with the amount being borrowed. The endpoint is responsible for creating the IOU double entries. Sample request payload {"lender": "jack","borrower": "john","amount": 7}
4. /delete - a DELETE method that accepts a username and deletes the users' record together with any IOUs related to the user. Sample request payload {"user": "jay"}

## Pre-requisites
You will need to have the following installed and configured appropriately on your computer.
* Nodejs
* Git
* yarn
* NB: You may need to manually install SQLITE3 if the `yarn` command fails to install it.


## Setup:

### Clone the project to your local drive
1. Navigate to your projects directory on your preferred terminal.
2. Run the command `git clone git@gitlab.com:william_w/iou.git` to clone the repository.

## Test and Run:

### Initialize your project
1. Still on your terminal, open your new projects' directory.
2. Run the command `yarn` to install the project dependencies.

    
### Running the project

1.  Run the command `yarn test` to execute all the unit tests.
2.  On your terminal, run the command `yarn start` to start the express server; you should see the message `Listening on port 8000!` if the server started successfully.
3.  You should also see the message `Database & tables created!`. This means that our API has successfully provisioned or connected to the database.
4.  Use the endpoints highlighted at the beginning of this readme to query your API.