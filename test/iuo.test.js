const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../app");
const should = chai.should();
const expect = chai.expect;
chai.use(chaiHttp);
const Promise = require("bluebird");
const chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
const {createPayload, iouPayload, deletePayload} = require('./__mocks__/payloads');

describe("iou_test ", function () {
  this.timeout(120 * 1000);
  const tests = [
    "Create user", "Create IOU", "Delete IOU and list the two set of users affected"
  ];

  const payloads = {'createPayload': createPayload, 'iouPayload': iouPayload, 'deletePayload': deletePayload};

  for (let index = 0; index < tests.length; index++) {
    it(tests[index], done => {
      let payload;
      if(index === 0)
        payload = JSON.parse(payloads['createPayload']);
      else if(index === 1)
        payload = JSON.parse(payloads['iouPayload']);
      else if(index === 2)
        payload = JSON.parse(payloads['deletePayload']);

      let event = [];
      for (var i = 0; i < payload.length; i++) {
        event.push(payload[i]);
      }

      Promise.mapSeries(event, e => {
        let eve = e;
        if (eve.request.method == "GET") {
          return chai
            .request(server)
            .get(eve.request.url)
            .set(eve.request.headers)
            .send(eve.request.body)
            .then(res => {
              return res;
            })
            .catch(err => {
              return err;
            });
        }

        if (eve.request.method == "POST") {
          return chai
            .request(server)
            .post(eve.request.url)
            .set(eve.request.headers)
            .send(eve.request.body)
            .then(res => {
              return res;
            })
            .catch(err => {
              return err;
            });
        }

        if (eve.request.method == "DELETE") {
          return chai
            .request(server)
            .delete(eve.request.url)
            .set(eve.request.headers)
            .send(eve.request.body)
            .then(res => {
              return res;
            })
            .catch(err => {
              return err;
            });
        }
      })
      .then(results => {
        for (let j = 0; j < results.length; j++) {
          let e = event[j];

          if (e.request.method == "GET") {
            results[j].should.have.status(e.response.status_code);
            let ar1 = results[j].body;
            let ar2 = e.response.body;
            if (e.response.status_code == 404) {
              continue;
            }
            expect(ar2.length).to.equal(ar1.length);
            for (let k = 0; k < ar1.length; k++) {
              expect(ar2[k]).to.deep.equal(ar1[k]);
            }
          }
          if (e.request.method == "POST") {
            expect(results[j].status).to.equal(e.response.status_code);
            let ar1 = results[j].body;
            let ar2 = e.response.body;
            if (e.response.status_code == 404) {
              continue;
            }
            expect(ar2.length).to.equal(ar1.length);
            for (let k = 0; k < ar1.length; k++) {
              expect(ar2[k]).to.deep.equal(ar1[k]);
            }
          }
          if (e.request.method == "DELETE") {
            expect(results[j].status).to.equal(e.response.status_code);
          }
        }
        done();
      })
      .catch(err => {
        done(err);
      });
    });
  }
});