const Sequelize = require('sequelize');
const UserModel = require('./models/user');

const sequelize = new Sequelize('iou', 'root', 'root', {
  dialect: 'sqlite',
  storage: './db/database.sqlite'
});

const User = UserModel(sequelize, Sequelize);

sequelize.sync({ force: false })
  .then(() => {
    console.log(`Database & tables created!`)
  })

module.exports = {User};