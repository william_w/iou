module.exports = (sequelize, type) => {
  return sequelize.define('user', {
      name: {
        type: type.STRING,
        primaryKey: true
      },
      owes: type.JSON,
      owed_by: type.JSON,
      balance: type.FLOAT
  },
  {timestamps: false});
}