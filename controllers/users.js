const { User } = require('../db/sequelize');

// record creation definition for /add POST end-point
exports.create_user = (req, res, next) => {
  // basic validation to check if all inputs have been provided
  let errors=[];
  if (!req.body.user){
    errors.push("No user provided");
    res.status(400).json({"error":errors});
    return;
  }
  else {
    // ensure names are saved as lower case to preserve primary key uniqueness
    User.findOrCreate({
      where: {name: req.body.user.toLowerCase()}
    })
    .then(user => {
      res.status(201).json(user[0]) // create user account only if it doesn't exist
    })
    .catch(err => {
      res.status(400).json({"error":err.message})
    });
  }
};

// record creation definition for /add POST end-point
exports.get_users = (req, res, next) => {
  // basic validation to check if all inputs have been provided
  let errors=[];
  const params = req.body.users;
  if (!params){
    errors.push("No user(s) provided");
    res.status(400).json({"error":errors});
    return;
  }
  else {
    if (!params instanceof Array || params.length < 1) {
      errors.push("Invalid user list provided");
      res.status(400).json({"error":errors});
      return;
    }
    else {
      User.findAll({
        where: {
          name: params
        }
      })
      .then(users => {
        res.status(200).json(
          users.length > 0 ? users.sort((a, b) => a.name - b.name) : {"users": ''}
        )
      })
      .catch(err => res.status(400).json({"error":err.message}))
    }
  }
};

// record creation definition for /add POST end-point
exports.create_iou = async (req, res, next) => {
  // basic validation to check if all inputs have been provided
  let errors=[];
  if (!req.body.lender){
    errors.push("No lender provided");
  }
  if (!req.body.borrower){
    errors.push("No borrower provided");
  }
  if (!req.body.amount /*|| !(parseInt(req.body.amount, 10) instanceof number)*/){
    errors.push("No amount provided");
  }
  
  if (errors.length){
    res.status(400).json({"error":errors.join(",")});
    return;
  }
  else {
    if (req.body.borrower.toLowerCase() === req.body.lender.toLowerCase()){
      res.status(400).json({"message": "lender and borrower must be two different users"});
      return;
    }

    const amount = parseFloat(req.body.amount);   
    let hasPriorDebt;

    const borrower = await User.findByPk(req.body.borrower.toLowerCase())
    .catch(err => res.status(400).json({"error":err.message}));

    const lender = await User.findByPk(req.body.lender.toLowerCase())
    .catch(err => res.status(400).json({"error":err.message}));

    if (!lender) {
      res.status(400).json({
        "message": `the lender ${req.body.lender} does not exist`
      });
      return;
    }
    if (!borrower) {
      res.status(400).json({
        "message": `the borrower ${req.body.borrower} does not exist`
      });
      return;
    }

    // check if borrower has prior debt
    hasPriorDebt = false
    if (lender.owed_by) {
      for(let key in lender.owed_by) {
        if (borrower.name === key) {
          hasPriorDebt = true;
        }
      }
    }

    if (hasPriorDebt) {
      // borrower does have prior debt; add the amount to the existing debt
      borrower.owes[lender.name] += amount;
      lender.owed_by[borrower.name] += amount;
    }
    else {
      // no prior debt; add the new debt record
      borrower.owes = {...borrower.owes, [lender.name]: amount};
      lender.owed_by = {...lender.owed_by, [borrower.name]: amount};
    }

    updateBalance(borrower)
    .then(balance => {
      User.update({ owes: borrower.owes, owed_by: borrower.owed_by, balance },
        { where: { name: borrower.name }})
      .catch(err => res.status(400).json({"error":err.message}));
    });

    updateBalance(lender)
    .then(balance => {
      User.update({ owes: lender.owes, owed_by: lender.owed_by, balance },
        { where: { name: lender.name }})
      .then(() => {
        User.findAll({
          where: {
            name: [borrower.name, lender.name]
          }
        })
        .then(users => {
          res.status(201).json(
            users.length > 0 ? users.sort((a, b) => a.name - b.name) : {"users": ''}
          )
        })
        .catch(err => res.status(400).json({"error":err.message}));
      })
      .catch(err => res.status(400).json({"error":err.message}));
    });
  }
};

// record creation definition for /add POST end-point
exports.delete_user = async (req, res, next) => {
  // basic validation to check if all inputs have been provided
  let errors=[];
  if (!req.body.user){
    errors.push("No user provided");
    res.status(400).json({"error":errors});
    return;
  }
  else {
    const user = await User.findByPk(req.body.user.toLowerCase())
    .catch(err => res.status(400).json({"error":err.message}));

    if (!user) {
      res.status(400).json({
        "message": `the lender ${req.body.user} does not exist`
      });
      return;
    }

    const removeRec = async (source, target) => {
      User.findByPk(source)
        .then((user) => {
          delete user.owes[target];
          delete user.owed_by[target];
          updateBalance(user)
            .then(balance => {
              User.update({ owes: user.owes, owed_by: user.owed_by, balance },
                { where: { name: user.name } })
            })
        })
        .catch(err => res.status(400).json({"error":err.message}));
    }

    // check if user has creditors
    if (user.owes) {
      for(let key in user.owes) {
        // if yes, delete the ious from the creditor's record
        await removeRec(key, user.name)
      }
    }

    // check if user has debtors
    if (user.owed_by) {
      for(let key in user.owed_by) {
        // if yes, delete the ious from the creditor's record
        await removeRec(key, user.name)
      }
    }

    User.destroy({
      where: {
        name: req.body.user.toLowerCase()
      }
    })
    .then(() => res.status(201).json({"message": "user and corresponding ious deleted successfully!"}))
    .catch(err => res.status(400).json({"error":err.message}));
  }
};

// update the borrower and lender balances
const updateBalance = async (user) => {
  let totalDebt = 0;
  let totalCredit = 0;
  if (user.owes) {
    for(let key in user.owes) {
      totalDebt += user.owes[key];
    }
  }

  if (user.owed_by) {
    for(let key in user.owed_by) {
      totalCredit += user.owed_by[key];
    }
  }
  return totalCredit - totalDebt;
};